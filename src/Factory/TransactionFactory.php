<?php
/**
 * Created by PhpStorm.
 * User: sohail
 * Date: 22/12/18
 * Time: 11:25 AM
 */
namespace App\Factory;

use App\Entity\Transaction;

class TransactionFactory
{
    public function makeDepositTransaction(int $amount): Transaction
    {
        $transaction = new Transaction($amount);
        $transaction->setType(Transaction::DEPOSIT);
        return $transaction;
    }
}
