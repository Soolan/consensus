<?php
/**
 * Created by PhpStorm.
 * User: sohail
 * Date: 21/12/18
 * Time: 4:03 PM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    const BASIC = 500;
    const INDIVIDUAL = 5000;
    const BUSINESS = 50000;
    const INSTITUTION = 500000;

    /**
     * @ORM\Column(type="integer")
     */
    private $limit;

    public function __construct($limit = 0)
    {
        $this->limit = $limit;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }
}
