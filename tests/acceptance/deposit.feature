Feature: deposit
  In order to trade
  As a verified user
  I need to deposit money

  Background:
    Given I am a verified user
    And I am on deposit page

  Scenario Outline: deposit via a saved account
    Given I have balance <balance>
    And I have daily deposit limit <limit>
    When I deposit <deposit>
    And my deposit does not exceed the limit <limit>
    Then I should see <total> as new balance

    Examples:
      | balance | deposit | limit | total |
      | 0       | 50      | 500   | 50    |
      | 450     | 850     | 500   | 450   |
      | 520     | 280     | 500   | 800   |

