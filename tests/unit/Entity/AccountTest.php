<?php
namespace App\Tests\Entity;
use App\Entity\Account;
class AccountTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSetLimit()
    {
        $account = new Account();
        $this->assertInstanceOf(Account::class, $account);
        $this->assertInternalType('int', $account->getLimit(), 'Hint: check the return type for getLimit()');
        $this->assertLessThanOrEqual(0 , $account->getLimit());

        $account->setLimit(Account::BASIC);
        $this->assertLessThanOrEqual(500, $account->getLimit());
    }
}
