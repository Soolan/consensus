<?php
namespace App\Tests\Entity;

use App\Entity\Transaction;

class TransactionTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testSetDepositAsTransactionType()
    {
        $transaction = new Transaction();
        $transaction->setType(Transaction::DEPOSIT);
        $this->assertEquals('deposit', $transaction->getType());
    }

    public function testSetDepositAmount()
    {
        $transaction = new Transaction(25);
        $this->assertEquals(25, $transaction->getAmount());
    }
}
