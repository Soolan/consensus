<?php namespace App\Tests\Entity;

use App\Entity\Account;
use App\Entity\User;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;
    protected $user;

    protected function _before()
    {
        $this->user = new User();
    }

    protected function _after()
    {
    }

    public function testSetAccount()
    {
        $this->user->setAccount(new Account());
        $this->assertInstanceOf(Account::class, $this->user->getAccount());
    }

    public function testSetTransaction()
    {

    }
}
