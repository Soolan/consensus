<?php namespace App\Tests\Factory;

use App\Entity\Transaction;
use App\Factory\TransactionFactory;

class TransactionFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests

    /**
     *
     */
    public function testCreateDepositTransaction()
    {
        $factory = new TransactionFactory();
        $deposit = $factory->makeDepositTransaction(125);
        $this->assertInstanceOf(Transaction::class, $deposit);
        $this->assertSame('deposit', $deposit->getType());
        $this->assertEquals(125, $deposit->getAmount());
    }
}
