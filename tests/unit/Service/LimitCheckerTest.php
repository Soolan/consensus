<?php
namespace App\Tests\Service;

use App\Entity\Account;
use App\Entity\Transaction;
use App\Entity\User;
use App\Service\LimitChecker;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

class LimitCheckerTest extends \Codeception\Test\Unit
{
    public function testDepositIsWithinDailyLimit()
    {
        $user = new User();
        $user->setAccount(new Account(Account::BASIC));
        $user->addTransaction(new Transaction(450));

        // Now, mock the repository so it returns the mock of the user
        $userRepository = $this->createMock(ObjectRepository::class);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user);

        // Last, mock the EntityManager to return the mock of the repository
        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $limitChecker = new LimitChecker($objectManager);
        $transactions = $userRepository->find(1)->getTransactions();

        $this->assertInstanceOf(User::class, $userRepository->find(1));
        $this->assertEquals(450, $transactions[0]->getAmount());
        $this->assertLessThanOrEqual($limitChecker->dailylimit(1), 450);
    }
}
