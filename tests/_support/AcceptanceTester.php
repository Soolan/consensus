<?php
namespace App\Tests;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * @Given I have balance :num1
     */
    public function iHaveBalance($num1)
    {
        throw new \Codeception\Exception\Incomplete("Step `I have balance :num1` is not defined");
    }

    /**
     * @Given I have daily deposit limit :num1:num2:num2
     */
    public function iHaveDailyDepositLimit($num1, $num2, $num3)
    {
        throw new \Codeception\Exception\Incomplete("Step `I have daily deposit limit :num1:num2:num2` is not defined");
    }

    /**
     * @When I deposit :num1:num2
     */
    public function iDeposit($num1, $num2)
    {
        throw new \Codeception\Exception\Incomplete("Step `I deposit :num1:num2` is not defined");
    }

    /**
     * @When my deposit does not exceed the limit :num1:num2:num2
     */
    public function myDepositDoesNotExceedTheLimit($num1, $num2, $num3)
    {
        throw new \Codeception\Exception\Incomplete("Step `my deposit does not exceed the limit :num1:num2:num2` is not defined");
    }

    /**
     * @Then I should see :num1:num2 as new balance
     */
    public function iShouldSeeAsNewBalance($num1, $num2)
    {
        throw new \Codeception\Exception\Incomplete("Step `I should see :num1:num2 as new balance` is not defined");
    }

    /**
     * @Given I am a verified user
     */
    public function iAmAVerifiedUser()
    {
        throw new \Codeception\Exception\Incomplete("Step `I am a verified user` is not defined");
    }

    /**
     * @Given I am on deposit page
     */
    public function iAmOnDepositPage()
    {
        throw new \Codeception\Exception\Incomplete("Step `I am on deposit page` is not defined");
    }

}
