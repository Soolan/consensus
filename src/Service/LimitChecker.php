<?php
/**
 * Created by PhpStorm.
 * User: sohail
 * Date: 29/12/18
 * Time: 1:19 PM
 */

namespace App\Service;


use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class LimitChecker
{
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function dailyLimit(int $id)
    {
        $userRepository = $this->objectManager
            ->getRepository(User::class);
        $user = $userRepository->find($id);
        $limit = $user->getAccount()->getLimit();

        return $limit;
    }
}
